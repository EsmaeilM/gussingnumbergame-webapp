"use strict";

let num01 = Math.trunc(Math.random() * 20);
let score = 20;
let highestScore = 0;
console.log(num01);

const DisplayMessege = function (className, messege) {
  document.querySelector(className).textContent = messege;
};

document.querySelector(".Chk").addEventListener("click", function () {
  const guessedNum = Number(document.querySelector(".input-01").value);

  if (guessedNum == num01) {
    document.querySelector("body").style.backgroundColor = "#60b347";
    document.querySelector(".box-1").textContent = num01.toString();
    DisplayMessege(".GusNum", "The number is Correct ✔");
    if (score > highestScore) {
      highestScore = score;
    }
  } else {
    if (guessedNum > num01) {
      DisplayMessege(
        ".GusNum",
        guessedNum - num01 > 10
          ? "The number is too High ❌"
          : "The number is High ❌"
      );
    } else {
      DisplayMessege(
        ".GusNum",
        num01 - guessedNum > 10
          ? "The number is too Low ❌"
          : "The number is Low ❌"
      );
    }
    document.querySelector("body").style.backgroundColor = "#FF0000";
    score--;
    document.querySelector(".Sc").textContent = score;
  }
});

document.querySelector(".plAg").addEventListener("click", function () {
  score = 20;
  document.querySelector(".Sc").textContent = score;
  document.querySelector(".hisC").textContent = highestScore;
  num01 = Math.trunc(Math.random() * 20);
  DisplayMessege(".box-1", "?");
  document.querySelector("body").style.backgroundColor = "#cac7c7";
  DisplayMessege(".GusNum", " Guessing the number ...");
  DisplayMessege(".input-01", "");
});
